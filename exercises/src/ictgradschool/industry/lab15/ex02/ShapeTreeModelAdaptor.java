package ictgradschool.industry.lab15.ex02;

import ictgradschool.industry.lab15.ex01.NestingShape;
import ictgradschool.industry.lab15.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * Created by wasi131 on 15/05/2017.
 */
public class ShapeTreeModelAdaptor implements TreeModel {
    NestingShape root;
    public ShapeTreeModelAdaptor(NestingShape root) {
        this.root = root;
    }

    @Override
    public Object getRoot() {
        return root;
    }

    @Override
    public Object getChild(Object parent, int index) {
//        if (shape instanceof NestingShape){
//            NestingShape nestingShape = (NestingShape) shape;
//            result = nestingShape.shapeCount();
//        }
        return ((NestingShape) parent).shapeAt(index);
    }

    @Override
    public int getChildCount(Object parent) {
//        int result = 0;
//        Shape shape = (Shape) parent;

//        if (shape instanceof NestingShape){
//            NestingShape nestingShape = (NestingShape) shape;
//            result = nestingShape.shapeCount();
//        }

        return ((NestingShape) parent).shapeCount();
    }

    @Override
    public boolean isLeaf(Object node) {
        if (node instanceof NestingShape) return false;
        return true;
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
//
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        return 0;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
//
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
//
    }
}
