package ictgradschool.industry.lab15.ex01;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wasi131 on 15/05/2017.
 */
public class NestingShape extends Shape {
    private List<Shape> shapes = new ArrayList<>();

    public NestingShape() {
    }

    public NestingShape(int x, int y) {
        super(x, y);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    @Override
    public void move(int width, int height){
        super.move(width, height);
        for(Shape s : shapes) {
            s.move(getWidth(), getHeight());
        }
    }

    @Override
    public void paint(Painter painter) {
        painter.drawRect(fX,fY,fWidth,fHeight);
        painter.translate(fX,fY);
        for(Shape s : shapes) {
            s.paint(painter);
        }
        painter.translate(-fX,-fY);
    }

    public void add(Shape shape) throws IllegalArgumentException {
        if (shape.parent() == null && (shape.getWidth() + shape.getX() < getWidth() || shape.getHeight() + shape.getY() < getHeight())) {
            shape.parent = this;
            shapes.add(shape);
        } else throw new IllegalArgumentException();
    }

    void remove(Shape child){
        if (contains(child)) {
            shapes.remove(indexOf(child));
            child.parent = null;
        }
    }

    public Shape shapeAt(int index) throws IndexOutOfBoundsException {return shapes.get(index);}

    public int shapeCount() {return shapes.size();}

    int indexOf(Shape child) {
        for (int i = 0; i < shapeCount(); i++) if (child.equals(shapeAt(i))) return i;
        return -1;
    }

    boolean contains(Shape child){return shapes.contains(child);}


}
